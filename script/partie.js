let socket = io('http://localhost:3000');
const urlServeur = 'http://localhost:8080';
socket.on('connection', function() {});
const tablePartie = document.getElementById('tablePartie').getElementsByTagName('tbody')[0];

getAllParties();

socket.on("refreshParties", (data) => {
    getAllParties();
})

function addPartie() {
    let partie = new Object;
    const partieName = document.getElementById("addPartieName");
    partie["name"] = partieName.value.toString();
    if (partie["name"].length > 0 && partie["name"].length < 255) {
        let request = new XMLHttpRequest();
        request.open("POST", urlServeur + '/partie', true);
        request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        request.onreadystatechange = function() {
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status === 201) {
                    socket.emit('refreshParties');
                    getAllParties();
                    partieName.value = '';
                } else {
                    alert("Erreur lors de la création de la partie");
                }
            }
        }
        request.send(JSON.stringify(partie));
    }
}

function getAllParties() {
    let request = new XMLHttpRequest();
    request.open('GET', urlServeur + '/parties', true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.onreadystatechange = function() {
        if (request.readyState === XMLHttpRequest.DONE) {
            if (request.status === 200) {
                tablePartie.querySelectorAll('*').forEach(n => n.remove());
                let data = JSON.parse(request.responseText);
                let nbPartieEnCours = 0;
                data.forEach(partie => {
                    addRowTablePartie(partie)
                    if (!partie.finish) {
                        nbPartieEnCours++;
                    }
                });
                document.getElementById('badgePartie').innerHTML = nbPartieEnCours;
            } else {
                alert("Erreur lors de la récupération des parties");
            }
        }
    }
    request.send();
}

function addRowTablePartie(partie) {
    // Insert a row in the table at the last row
    let newRow = tablePartie.insertRow();
    newRow.id = partie.id;
    newRow.innerHTML = `<td><input id=` + partie.id + ` type="checkbox" class="select-checkbox" onchange="updatePartie(this);"` + (partie.finish ? `checked=""` : "") + `></td>
                        <td><label class="label label-default  ` + (partie.finish ? "strike" : "") + `">` + partie.name + `</label></div></td>
                        <td><button  id=` + partie.id + `  class="btn btn-danger" onclick="deletePartie(this);" ><i class="fa fa-trash"></i></button></td>
                        `
}

function updatePartie(checkbox) {
    let partie = new Object;
    partie["finish"] = checkbox.checked;
    let request = new XMLHttpRequest();
    request.open("PUT", urlServeur + '/partie/' + checkbox.id, true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.onreadystatechange = function() {
        if (request.readyState === XMLHttpRequest.DONE) {
            if (request.status === 200) {
                socket.emit('refresh');
                getAllParties();
            } else {
                alert("Erreur lors de la modification de la partie");
            }
        }
    }
    request.send(JSON.stringify(partie));
}


function deletePartie(button) {
    let request = new XMLHttpRequest();
    request.open("DELETE", urlServeur + '/partie/' + button.id, true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.onreadystatechange = function() {
        if (request.readyState === XMLHttpRequest.DONE) {
            if (request.status === 200) {
                socket.emit('refresh');
                getAllParties();
            } else {
                alert("Erreur lors de la suppression de la partie");
            }
        }
    }
    request.send();
}