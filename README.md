# Front bayfoot Manager

Front-end de l'application bayfoot Manager.
BabyFoot Manager est une application web de type RIA permettant de créer des parties de babyfoot. Sa particularité sera de pouvoir créer des parties de manière collaborative.

## Prérequis

* Serveur web démarrer sur votre machine


### Pour commencer 

Démarrer le back-end : [back-end](https://gitlab.com/babyfoot-manager/back).

Puis, récupérer les sources dans le dosier "www" de votre serveur web

```
git clone https://gitlab.com/babyfoot-manager/front
cd front
npm install
```

### Lancer l'application

Aller dans le dossier front sur un navigateur.

### Résultat :

![Alt text](img/applicationVide.PNG?raw=true)
![Alt text](img/application.PNG?raw=true)
![Alt text](img/chat.PNG?raw=true)
