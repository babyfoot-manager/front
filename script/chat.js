const chats = document.getElementById('chatMessages');

getAllChats();


socket.on("refreshChats", (data) => {
    getAllChats();
});

socket.on("typingStart", (data) => {
    typingStart(data);
});

socket.on("typingEnd", (data) => {
    typingEnd();
});


function addChat() {
    let chat = new Object;
    const message = document.getElementById("addChatMessage")
    chat["message"] = message.value.toString();
    chat["userId"] = userId;
    if (chat["message"].length > 0 && userId > 0) {
        let request = new XMLHttpRequest();
        request.open("POST", urlServeur + '/chat', true);
        request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        request.onreadystatechange = function() {
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status === 201) {
                    socket.emit('refreshChats');
                    socket.emit('typingEnd');
                    getAllChats();
                    message.value = '';
                } else {
                    alert("Erreur lors de la création de la partie");
                }
            }
        }
        request.send(JSON.stringify(chat));
    } else {
        alert("Veuillez saisir un message et votre nom")
    }
}

function getAllChats() {
    let request = new XMLHttpRequest();
    request.open('GET', urlServeur + '/chats', true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.onreadystatechange = function() {
        if (request.readyState === XMLHttpRequest.DONE) {
            if (request.status === 200) {
                chats.querySelectorAll('*').forEach(n => n.remove());
                let data = JSON.parse(request.responseText);
                data.forEach(message => {
                    addChatMessage(message);
                });
                chats.scrollTop = chats.scrollHeight;
            } else {
                alert("Erreur lors de la récupération des chats");
            }
        }
    }
    request.send();
}

function addChatMessage(message) {
    const date = new Date(message.message_date)
    chats.innerHTML += `<div class="d-flex justify-content-` + (message.user_id == userId ? "end" : "start") + ` mb-4">` +
        (message.user_id == userId ? `<div class="msg_cotainer_send">` : `<div class="msg_cotainer">`) +
        message.message + `
        <span class="msg_time">` + (message.user_id != userId ? message.user_name + `, ` : "") +
        date.getDate() + "/" + (date.getMonth() + 1) + " " + date.getHours() + ":" + date.getMinutes() +
        `</span></div></div>`
}

function typingMessage() {
    let user = new Object;
    user["name"] = document.getElementById("addUserName").value.toString();
    message = document.getElementById("addChatMessage").value.toString();
    if (message.length > 0 && user["name"].length > 0) {
        socket.emit('typingStart', user);
    } else {
        socket.emit('typingEnd');
    }
}

function typingStart(data) {
    typing = document.getElementById("typing");
    typing.innerHTML = data.name + " est en train d'écrire un message...";
}

function typingEnd() {
    typing = document.getElementById("typing");
    typing.innerHTML = "";
}